const webpack = require('webpack');
const path = require('path');

const config = {
  entry: [
    'babel-polyfill',
    'react-hot-loader/patch',
    './src/index.js'
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/i,
        exclude: /\.module\.css$/i,
        use: [
          'style-loader',
          'css-loader'
        ],       
      },
      {
        test: /\.module\.css$/i,
        use: [
          'style-loader',
          {
            loader: 'style-loader',
            options: {
              modules: true,
              injectType: 'lazyStyleTag'
            }
          },
          'css-loader',
        ],
        include: /\.module\.css$/
      }
    ]
  },
  resolve: {
    extensions: [
      '.js',
      '.jsx'
    ],
    alias: {
      'react-dom': '@hot-loader/react-dom'
    },
    extensions: [".js", ".jsx", ".json", ".ts", ".tsx"],// other stuff
    fallback: {
      "fs": false,
      "path": require.resolve("path-browserify"),
      // "buffer": require.resolve("buffer/")
    }
  },
  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
  }
};

module.exports = config;
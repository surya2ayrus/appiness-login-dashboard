import React, { Component } from 'react';
import LoginPageUi from './LoginPageUi';
import '../../../css/loginPage.css';
import { connect } from 'react-redux';
import { LOGIN } from '../../Action/Action';

class LoginPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: "",
            password: "",
            isLoggedIn: false
        }

        this.userCredentials = {
            username: "hruday@gmail.com",
            password: "hruday123"
        }
    }

    usernameChangeHandler = (event) => {
        // console.log("inside username",event);
        this.setState({
            username: event.target.value
        })
    }

    passwordChangeHandler = (event) => {
        // console.log("inside password",event);
        this.setState({
            password: event.target.value
        })
    }

    loginHandler = (event) => {
        console.log("logged in", event);
        if (this.userCredentials.username != this.state.username || this.userCredentials.password != this.state.password) {
            if (!this.state.username || !this.state.password) {
                this.setState({
                    error: "enter username and password"
                })
            } else {
                this.setState({
                    error: "credentials do not match"
                })
            }
        }


        if (this.userCredentials.username == this.state.username && this.userCredentials.password == this.state.password) {
            console.log("props", this.props);
            this.props.dispatch({ type: LOGIN })
            this.setState({
                isLoggedIn: true
            })
            console.log("inside props",this.props);
            this.props.history.push('/dashboard')
        }

    }


    render() {
        let errorMsg;
        if (this.state.error) {
            errorMsg = <span className="error-msg"> {this.state.error} </span>
        }
        return (
            <div className="text-center">
                <LoginPageUi usernameChangeHandler={this.usernameChangeHandler} passwordChangeHandler={this.passwordChangeHandler} loginHandler={this.loginHandler} />
                <div className="text-center">
                    {errorMsg}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps)(LoginPage)

import React, { Component } from 'react'

class LoginPageUi extends Component {
    render() {
        return (
            <div className="container jumbotron d-flex flex-column justify-content-center login-container">
                <div className="input-group mb-3 row">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Username</span>
                    </div>
                    <input type="text" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" onChange={this.props.usernameChangeHandler} required />
                </div>

                <div className="input-group mb-3 row">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Password</span>
                    </div>
                    <input type="password" className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" onChange={this.props.passwordChangeHandler} required />
                </div>

                <div>
                    <button className="btn btn-outline-primary btn-width" onClick={this.props.loginHandler}> Login </button>
                </div>
            </div>
        )
    }
}

export default LoginPageUi

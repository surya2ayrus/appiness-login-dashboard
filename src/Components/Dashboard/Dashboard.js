import React, { Component } from 'react'
import '../../../css/dashboard.css'
const json = require('../../../DashboardData.json');


class Dashboard extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        let displayImageNotes = json.user.map(post => {
            return <div className="singlePostContainer" key={post.id}>
                <div className="post" key={post.id}>
                    <h2>{post.name}</h2>
                    <p> Age: {post.age}</p>
                    <p> Gender: {post.gender}</p>
                    <p> Email: {post.email}</p>
                    <p> PhoneNo: {post.phoneNo}</p>
                </div>
            </div>
        })
        return (
            <div>
                <div className="container-fluid text-center dashboard-header">
                    Dashboard
                </div>
                <div className="postsContainer">
                    {displayImageNotes}
                </div>
            </div>
        )
    }
}

export default Dashboard

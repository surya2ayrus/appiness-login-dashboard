import {LOGIN} from '../Action/Action';

const reducer = function (state,action){
    console.log("inside reducer");
    switch(action.type){
        case LOGIN:
            let loginCheck = JSON.parse(localStorage.getItem('login'))
            if(!loginCheck || !loginCheck.login){
                localStorage.setItem('login',JSON.stringify({"login": "true"}))
            }  
            return {...state, isLoggedIn: true}
        default:
            return state
    }
}
export default reducer;
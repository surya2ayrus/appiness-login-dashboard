import React from "react";
import { hot } from 'react-hot-loader/root';
import LoginPage from './Components/Login/LoginPage'
import Dashboard from './Components/Dashboard/Dashboard';
import { connect } from 'react-redux'
import '../css/loginPage.css';
import {
   BrowserRouter as Router,
   Switch,
   Route,
   Link
} from "react-router-dom";

class App extends React.Component {

   render() {

      return (
         <Router history={history}>
            <div>
               <div className="company-name">
                  Appiness
               </div>
               <Switch>
                  <Route path="/dashboard" component={Dashboard}/>
                     
                  <Route exact path="/" component={LoginPage}/>
               </Switch>
            </div>
         </Router>
      );
   }
}

const mapStateToProps = (state) => {
   return state;
}

export default connect(mapStateToProps)(hot(App));
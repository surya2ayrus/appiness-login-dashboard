import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import {Provider} from "react-redux";
import store from './Store/Store';

var mountNode = document.getElementById("app");
ReactDOM.render(<Provider store={store}>
    <App />
    </Provider>, mountNode);
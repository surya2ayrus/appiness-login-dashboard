const { createStore } = require('redux');
import reducer from '../Reducer/Reducer';

const store = createStore(reducer,{
    isLoggedIn: false,
})

export default store